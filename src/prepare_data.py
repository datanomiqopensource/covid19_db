#### imports and load data
import pandas as pd
import streamlit as st

# change the branch name where necesarry
DATA_URL = 'https://gitlab.com/datanomiqopensource/covid19_db/-/raw/master/datasets/covid19.csv'
#DATA_URL_STOCKS = 'https://gitlab.com/datanomiqopensource/covid19_db/-/raw/master/datasets/stocks_data.csv'
DATA_URL_CONTINENTS = 'https://gitlab.com/datanomiqopensource/covid19_db/-/raw/master/datasets/continents_dict.csv'
DATA_URL_POPULATIONS = 'https://gitlab.com/datanomiqopensource/covid19_db/-/raw/master/datasets/world_population_dict.csv'
DATA_URL_ISO = 'https://gitlab.com/datanomiqopensource/covid19_db/-/raw/master/datasets/ISO_code_dict.csv'


def load_data():
    data = pd.read_csv(DATA_URL)
    data['Date'] = pd.to_datetime(data['Date']).dt.date
    data['ActiveCases'] = data['ConfirmedCases'] - data['Deaths'] - data['RecoveredCases']
    data.drop(columns = ['Lat', 'Long', 'script_runtime', 'updated_at'])
    # get rid of 'State'
    data = data.groupby(['Country', 'Date'], as_index=False).agg(
        {'ConfirmedCases' : 'sum','Deaths' : 'sum', 
         'RecoveredCases' : 'sum', 'ActiveCases' : 'sum'})
    return data

def create_new_cases(data):
    list_of_countries = list(set(data['Country']))
    list_of_countries.sort()
    data['NewSick'] = None
    data['NewDeaths'] = None
    data['NewRecovered'] = None
     
    data = data.set_index(['Date', 'Country']) #create multiindex df
    for element in list_of_countries: 
        date_iterator = latest_date
        while date_iterator != oldest_date:
            data['NewSick'][date_iterator, element] = (data['ConfirmedCases'][date_iterator, element ] 
                                                         - data['ConfirmedCases'][date_iterator-pd.Timedelta(1, unit='d'), element])
            data['NewDeaths'][date_iterator, element] = (data['Deaths'][date_iterator, element ] 
                                                         - data['Deaths'][date_iterator-pd.Timedelta(1, unit='d'), element])
            data['NewRecovered'][date_iterator, element] = (data['RecoveredCases'][date_iterator, element ] 
                                                         - data['RecoveredCases'][date_iterator-pd.Timedelta(1, unit='d'), element])
            if date_iterator != oldest_date:
                date_iterator -= pd.Timedelta(1, unit='d')                                                                              
            
    data.index = data.index.set_names(['Date', 'Country'])
    data = data.reset_index() #reset multiindex df
    return data

def add_continents(data):
    #data_continents = pd.read_csv('continents_dict.csv')
    data_continents = pd.read_csv(DATA_URL_CONTINENTS)
    data = data.merge(data_continents, on = 'Country', how = 'left', copy = False)
    data = data.dropna(subset = ['Continent'])
    return data

def add_populations(data):
    #data_populations = pd.read_csv('world_population_dict.csv')
    data_populations = pd.read_csv(DATA_URL_POPULATIONS, 
                                    usecols = ['Country',
                                              'Population', 
                                              'Density', 
                                              'Med. Age'])
    data = data.merge(data_populations, on = 'Country', how = 'left', copy = False)
    data = data.dropna(subset = ['Population'])
    return data

def add_ISO(data):
    #data_iso = pd.read_csv('ISO_code_dict.csv')
    data_iso = pd.read_csv(DATA_URL_ISO)
    data = data.merge(data_iso, on = 'Country', how = 'left', copy = False)
    return data

# def load_stocks():
#     data = pd.read_csv(DATA_URL_STOCKS)
#     return data

data = load_data()

latest_date =  data['Date'].max()
oldest_date = data['Date'].min()
list_of_countries = list(set(data['Country']))
list_of_countries.sort()


data = add_continents(data)
data = add_populations(data)
data = add_ISO(data)
#data_stocks = load_stocks()

data_latest = data[data['Date'] == pd.to_datetime(latest_date)]
data_latest['Rank_all'] = data_latest['ConfirmedCases'].rank(method = 'first', ascending  = False)
data_latest['Rank_active'] = data_latest['ActiveCases'].rank(method = 'first', ascending  = False)

data = create_new_cases(data)
# list_of_stocks = list(set(data_stocks['stocks']))
# list_of_stocks.sort()

data.to_csv('../datasets/covid19_data.csv')
data_latest.to_csv('../datasets/covid19_data_latest.csv')