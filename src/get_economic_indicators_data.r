
BASE_URL = "https://www.mql5.com/en/economic-calendar/"

countries = c("germany")


categories = c("10-year-bond-auction","2-year-note-auction","30-year-bond-auction",
         "5-year-note-auction","gdp-qq","gdp-yy","unemployment-rate","unemployment-change",
         "cpi-mm","cpi-yy","unemployment-nsa","unemployment-rate",
         "export-price-index-mm","export-price-index-yy","hicp-mm","hicp-yy",
         "import-price-index-mm","import-price-index-yy","ppi-mm","ppi-yy",
         "wpi-mm","wpi-yy","current-account-nsa","exports-mm",
         "imports-mm","trade-balance","trade-balance-nsa","factory-orders-mm",
         "factory-orders-yy","ifo-business-climate-indicator","ifo-business-expectations",
         "ifo-current-business-situation","industrial-production-mm","industrial-production-yy",
         "markit-composite-pmi","markit-manufacturing-pmi","markit-services-pmi",
         "new-car-registrations-mm","new-car-registrations-yy","zew-economic-sentiment-indicator",
         "zew-economic-situation","gfk-consumer-climate","retail-sales-mm","retail-sales-yy")


#####################
# Generate urls
####################


urls = apply(expand.grid(BASE_URL, countries,paste("/",categories,sep = '')), 1, paste, collapse="")


###################################
# fetch data from the urls into df
##################################

data <- lapply(urls, function(urls) {
  read.csv(paste(urls,"/export",sep = ''),sep = "\t")
})
 

###################################
# add categories to each dataframe
##################################

for (i in 1:length(data)) {
  
  data[[i]]$type <- categories[i]
    
}


library(plyr)
economic_indicators = ldply(data)


############################
# revalue some level names
############################

CleanType <- function(df) {
  df$type <- recode(df$type,
                             "cpi-mm" = "Consumer Price Index (CPI) Monthly", 
                             "cpi-yy" = "Consumer Price Index (CPI) Yearly",
                              "gdp-qq" = "Gross Domestic Product (GDP) Quarterly",
                              "gdp-yy" = "Gross Domestic Product (GDP) Yearly",
                                "hicp-mm" = "Harmonised Index of Consumer Prices (HICP) Monthly",
                                "hicp-yy" = "Harmonised Index of Consumer Prices (HICP) Yearly",
                                "ppi-mm" = "Producer Price Index (PPI) Monthly",
                                "ppi-yy" = "Producer Price Index (PPI) Yearly",
                                "markit-composite-pmi" = "Composite Purchasing Managers Index (PMI)",
                                "markit-manufacturing-pmi" = "Manufacturing Purchasing Managers Index (PMI)",
                                "markit-services-pmi" = "Services Purchasing Managers Index (PMI)",
                                "wpi-mm" = "Wholesale Price Index Monthly",
                                "wpi-yy" ="Wholesale Price Index Yearly",
                             .default = df$type)
  
  return(df)
}





economic_indicators = CleanType(economic_indicators)

economic_indicators$type = gsub("-"," ",economic_indicators$type)
economic_indicators$type = gsub("mm","Monthly",economic_indicators$type)
economic_indicators$type = gsub("yy","Yearly",economic_indicators$type)
economic_indicators$type = str_to_title(economic_indicators$type)



economic_indicators$sector = ifelse(str_detect(economic_indicators$type,"Auction"),"Market",
                                    ifelse(str_detect(economic_indicators$type,"Gross"),"GDP",
                                           ifelse(str_detect(economic_indicators$type,"Unemployment"),"Labour",
                                                  ifelse(str_detect(economic_indicators$type,"Price"),"Prices",
                                                         ifelse(str_detect(economic_indicators$type,"Exports|Imports|Trade|Current Account"),"Trade",
                                                                ifelse(str_detect(economic_indicators$type,"Factory|Business|Production|Purchasing|Car|Zew"),"Business",
                                                                       ifelse(str_detect(economic_indicators$type,"Gfk|Retail"),"Consumer","Other")))))))

filename = paste0("datasets/", "economic_indicators.csv")

write.csv(economic_indicators,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())

