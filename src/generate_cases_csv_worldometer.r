library("rvest")
url <- "https://www.worldometers.info/coronavirus/#countries"

cases <- url %>%
  html() %>%
  html_nodes(xpath='//*[@id="main_table_countries_today"]') %>%
  html_table()


cases <- cases[[1]]

library(dplyr)
cases = cases %>%
  mutate_all(funs(gsub("[[:punct:]]", "", .)))



filename = paste0("datasets/", "covid19_latest.csv")

write.csv(cases,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())
