##############################
# Define start and end date
#############################

start <- as.Date("2020-01-22") # only For the first time 
#stocks_data = read.csv("datasets/stocks_data.csv")

#start <- max(as.Date(stocks_data$updated_at))
#rm(stocks_data)
end <- as.Date(Sys.Date())



stocks = paste0(c("BAYN","FME",
                   "EOAN","LHA","LIN",
                   "RWE","ADS","DPW",
                   "VOW3","HEN3","1COV",
                   "DBK","VNA","SAP",
                   "BAS","BMW","DTE",
                   "CON","SIE","IFX",
                   "MUV2","MRK","DB1",
                  "DAI","TKA","FRE",
                  "ALV","WDI"),".DE")

stocknames = read.csv("datasets/StockNames.csv")
stocks = c(stocks,levels(stocknames$stock_name))
rm(stocknames)

for (i in 1:length(stocks)) {
  tryCatch({
  getSymbols(stocks[i], src = "yahoo", from = start, to = end)
  }, error=function(e){})
}



xtstodf <- function(xts_obj) {
  
  df =  data.frame(date=index(xts_obj), coredata(xts_obj))
  return(df)
  
}

list2env(lapply(Filter(is.xts, as.list(.GlobalEnv)), xtstodf), envir = .GlobalEnv)


clean_cols <- function(df) {
  
   colnames(df) = c("date","open","high",
                   "low","close","volume",
                   "adjusted")
  
  return(df)
  
}


list2env(lapply(Filter(is.data.frame, as.list(.GlobalEnv)), clean_cols), envir = .GlobalEnv)

dflist <- Filter(is.data.frame, as.list(.GlobalEnv))

dflist <- Map(cbind, dflist, stocks = names(dflist))


stocks_data<- rbindlist(dflist)

stocks_data$updated_at = max(stocks_data$date)

stocks_data$script_runtime <- Sys.time()

stocknames = read.csv("datasets/StockNames.csv")

stocks_data = stocks_data %>% left_join(stocknames,by = c("stocks"="stock_name"))
stocks_data$sector = as.character(stocks_data$sector)
stocks_data$sector[is.na(stocks_data$sector)] <- "DAX"

filename = paste0("datasets/", "stocks_data.csv")

write.csv(stocks_data,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())

